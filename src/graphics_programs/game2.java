package graphics_programs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;


@SuppressWarnings("serial")
public class game2  extends JPanel implements ActionListener {
	public boolean leftdirection=false;
	public boolean rightdirection=false;
	public boolean GAME=true;
	public int x=50,y=255,x1,y1=0,counter=0;
	private Image basket,johnny;
	 private Timer timer;
	 public game2() { 
		    addKeyListener(new movingbasket());
	        setBackground(new Color(0,153,255));
	        setFocusable(true);
	        setPreferredSize(new Dimension(300,300));
	        loadImages();
	        timer = new Timer(140, this);
	        timer.start();
	 }
	 public void paintComponent(Graphics g) {   
	        super.paintComponent(g);    
	        basket(g);
	    }
	 public void loadImages(){
		 ImageIcon iid = new ImageIcon("C:\\Users\\Priyanka Miranda\\workspace1\\graphics_programs\\src\\graphics_programs\\t\\fruitbasket.jpg");
	        basket = iid.getImage();
	        ImageIcon iib = new ImageIcon("C:\\Users\\Priyanka Miranda\\workspace1\\graphics_programs\\src\\graphics_programs\\t\\johnny.png");
	        johnny = iib.getImage();
			 leftdirection=false;
			 rightdirection=false;
			 if(y1==0){
			 int r = (int) (Math.random() * 24);
		         x1= ((r * 10));
			 }
	 }
	 public void basket(Graphics g){
            g.drawImage(johnny,x1,y1,this);
            g.drawImage(basket,x,y , this);
            
            } 
	 private void caught() { 
		 for(int s=0;s<20;s++){
	        if (x==x1+s||x==x1-s) {                                                      
	          y1=0;                                        
	           loadImages();
	           break;
	        }
	        
		 }
	       
	    } 
	    @Override
	    public void actionPerformed(ActionEvent e) {
	    	if (GAME) { 
	    	caught();
	    	move();
	    	drop();
	    	}
	        repaint(); 
	    }
	    private void drop(){
	    	y1=y1+5;
	    }
private void move(){
	if(leftdirection)
	x-=10;
	if(rightdirection)
		x+=10;
}

private class movingbasket extends KeyAdapter { 
    @Override                              
    public void keyPressed(KeyEvent e) { 
    	int key = e.getKeyCode();
        if ((key == KeyEvent.VK_LEFT) ){
            leftdirection = true;  
            rightdirection = false;
                                    
        }
        if ((key == KeyEvent.VK_RIGHT)) {
            rightdirection = true;
            leftdirection = false;
           
        }
       
    }
}
}